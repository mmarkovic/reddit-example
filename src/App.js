import React, { Component } from 'react'
import axios from 'axios'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import { withStyles } from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Avatar from '@material-ui/core/Avatar'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import './App.css'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
})

class App extends Component {
  constructor(props) {
    super(props)

    this.classes = props.classes
    this.state = {
      posts: [],
    }
  }

  componentWillMount() {
    axios.get('https://www.reddit.com/r/popular.json')
      .then((resp) => {
        this.setState({posts: resp.data.data.children})
      })
      .catch((err) => {
        console.log(err)
      })
  }

  render() {
    console.log(this.state.posts)
    const postList = this.state.posts.map((post) => {
      const parsedImgUri = post.data.preview.images[0].source.url.replace("&amp;", "&")
      return <ListItem button key={post.data.id}>
        <Avatar alt="" src={parsedImgUri} />
        <ListItemText primary={post.data.title} />
      </ListItem>
    })

    return (
      <Grid container spacing={24}>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Title
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid item xs={12}>
          <Paper className={this.classes.paper}>
            <List component="nav">
              {postList}
            </List>
          </Paper>
        </Grid>
      </Grid>
   )
  }
}

export default withStyles(styles)(App)
